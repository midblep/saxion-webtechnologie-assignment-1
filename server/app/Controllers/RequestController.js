const express = require('express');
const router = express.Router();

const requestProvider = require(__basedir + "/app/Providers/RequestProvider");

const IsAdmin = require(__basedir + "/app/Middleware/IsAdminMiddleware");
const IsUser = require(__basedir + "/app/Middleware/IsUserMiddleware");
const Authenticate = require(__basedir + "/app/Middleware/AuthenticateMiddleware");


// routes for this controller
router.route('')
    .get(Authenticate, IsUser, function (req, res) {
        const created_by = req.query.created_by ?? undefined;

        if(created_by !== undefined) {
            requestProvider.getAllCreatedBy(created_by, function (err, results) {
                if (!err) {
                    if(results == undefined) {
                        res.status(404).json("No requests found!");
                        return;
                    } else {
                        res.status(200).json(results);
                        return;
                    }
                } else {
                    res.status(400).json(err.message);
                    return;
                }
            });
        }
        else {
            requestProvider.getAll(function (err, results) {
                if (!err) {
                    res.status(200).json(results);
                    return;
                } else {
                    res.status(400).json(err.message);
                    return;
                }
            });
        }
    })

    .post(Authenticate, IsUser, function (req, res) {
        const newRequest = req.body;

        if (!newRequest.created_by) return res.status(400).json("Invalid Body");

        requestProvider.existsFromUser(newRequest.created_by, (result) => {
            if (result) {
                return res.status(400).json("A request from this user already exists");
            } else {
                requestProvider.create(false, Date.now(), newRequest.created_by, (err, request) => {
                    if (!err && request !== undefined) {
                        return res.status(201).json(request);
                    } else {
                        return res.status(500).json(err);
                    }
                });
            }
        });
    });

router.route('/:id')
    .get(Authenticate, IsUser, function (req, res) {
        if (!req.params.id) return res.status(400).json("Missing Query Parameter id");

        requestProvider.getById(req.params.id, function (err) {
            if (!err) {
                res.status(200).json("Updated");
                return;
            } else {
                res.status(400).json(err.message);
                return;
            }
        });
    })

    .put(Authenticate, IsAdmin, function (req, res) {
        if (!req.params.id) return res.status(400).json("Missing Query Parameter id");
        if (!req.body.approved) return res.status(400).json("Invalid Body");

        requestProvider.updateApproved(req.query.id, req.body.approved, function (err, results) {
            if (!err) {
                return res.status(200).json(results);
            } else {
                return res.status(400).json(err.message);
            }
        });
    })

    .delete(Authenticate, IsAdmin, function (req, res) {
        if (!req.params.id) return res.status(400).json("Missing Query Parameter id");

        requestProvider.delete(req.params.id, function (err) {
            if (!err) {
                res.status(200).json("Deleted");
                return;
            } else {
                res.status(400).json(err.message);
                return;
            }
        });
    });


module.exports = router