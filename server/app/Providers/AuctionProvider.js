const db = require(__basedir + "/app/Services/DataService").connect;

module.exports.getAuctionById = function(id, callback) {
    db.get(`select * from auctions where id = '${id}'`, [], (err, auction) => {
        callback(err, auction);
    });
}

module.exports.getAuctionByTitle = function(title, callback) {
    db.get(`select * from auctions where title = '${title}'`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.getAllAuctions = function(callback) {
    db.all(`select * from auctions`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.getAllAuctionsPromised = async function() {
    return new Promise((resolve, reject) => {
        db.all(`select * from auctions`, [], (err, user) => {
            if(err) return reject(err);
            resolve(user);
        });
    })
}

module.exports.createAuction = function(auction, callback) {
    const component = this;
    db.run(`INSERT INTO auctions (title, description, end_date, user_id, created_by) VALUES (?,?,?,?,?)`, [auction.title, auction.description, auction.end_date, auction.user_id, auction.created_by], function(err) {
        component.getAuctionById(this.lastID, (err, auction) => {
            callback(err, auction);
        })
    });
}

module.exports.editAuction = function(auction, callback) {
    const component = this;
    db.run(`UPDATE auctions SET title = '${auction.title}', description = '${auction.description}', end_date = '${auction.end_date}', user_id = '${auction.user_id}' WHERE id = '${auction.id}'`, function(err) {
        component.getAuctionById(auction.id, (err, auction) => {
            callback(err, auction);
        })
    });
}

module.exports.deleteAuction = function(id, callback) {
    db.get(`delete from auctions where id = '${id}'`, [], (err, auction) => {
        callback(err, auction);
    });
}

