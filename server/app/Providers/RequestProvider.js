const db = require(__basedir + "/app/Services/DataService").connect;

module.exports.getAll = function (callback) {
    db.all(`select * from requests`, [], (err, requests) => {
        callback(err, requests);
    });
}

module.exports.getAllCreatedBy = function (id, callback) {
    db.all(`select * from requests where created_by = '${id}'`, [], (err, requests) => {
        callback(err, requests);
    })
}

module.exports.getById = function (id, callback) {
    db.get(`select * from requests where id = '${id}'`, [], (err, request) => {
        callback(err, request);
    });
}

module.exports.delete = function (id, callback) {
    db.run(`delete from requests where id = '${id}'`,
    (err) => {
        callback(err);
    });
}

module.exports.getByCreated = function (id, callback) {
    db.get(`select * from requests where created_by = '${id}'`, [], (err, request) => {
        callback(err, request);
    });
}

module.exports.getByUser = function (user_id, callback) {
    db.get(`select * from requests where created_by = '${user_id}'`, [], (err, request) => {
        callback(err, request);
    })
}

module.exports.create = function (approved, createdAt, createdBy, callback) {
    const component = this;

    db.run(`INSERT INTO requests (approved, created_at, created_by) VALUES (?,?,?)`, [approved, createdAt, createdBy], (err) => {
        if (!err) {
            component.getByCreated(createdBy, (err, request) => {
                if (!err) return callback(err, request);
            })
        } else {
            return callback(err);
        }
    });
}

module.exports.updateApproved = function (id, approved, callback) {
    db.run(`UPDATE requests SET approved = '${approved}' WHERE id = '${id}'`, (err) => {
        this.getById(id, (error, result) => {
            if(!error) callback(err, result);
        })
        callback(err);
    });
}

module.exports.existsFromUser = function (userId, callback) {

    this.getByCreated(userId, (err, result) => {
        if(result !== undefined) {
            return callback(true);
        } else {
            return callback(false);
        }
    });
}