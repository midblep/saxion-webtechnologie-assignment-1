const db = require(__basedir + "/app/Services/DataService").connect;

module.exports.getBidById = function (id, callback) {
    db.get(`select *
            from bids
            where id = '${id}'`, [], (err, bids) => {
        callback(err, bids);
    });
}

module.exports.getAllBids = function (callback) {
    db.all(`select *
            from bids`, [], (err, bid) => {
        callback(err, bid);
    });
}

module.exports.getAllBidsForAuction = function (auction_id, callback) {
    db.all(`select * from bids where auction_id='${auction_id}'`, [], (err, bids) => {
        callback(err, bids);
    });
}

module.exports.bidsFromAuction = function (auction_id, callback) {
    db.all(`select *
            from bids
            where auction_id = '${auction_id}'`, [], (err, bids) => {
        callback(err, bids);
    });
}

module.exports.bidsFromAuctionPromised = async function (auction_id) {
    return new Promise((resolve, reject) => {
        db.all(`select *
            from bids
            where auction_id = '${auction_id}'`, [], (err, bids) => {
            if(err) return reject(err);
            resolve(bids);
        });
    });
}

module.exports.createBid = function (newBid, callback) {
    const component = this;

    db.run('INSERT INTO bids (auction_id, amount, created_by, created_at) VALUES (?,?,?,?)',
        [newBid.auction_id, newBid.amount, newBid.created_by, newBid.created_at], function (err) {
            if (!err && this.lastID !== null) {
                component.getBidById(this.lastID, (err, bid) => {

                    if (!err) return callback(err, bid);
                })
            } else {
                return callback(err);
            }
        });
}

module.exports.deleteBid = function (id, callback) {
    db.get(`delete
            from bids
            where id = '${id}'`, [], (err) => {
        callback(err)
    });
}























