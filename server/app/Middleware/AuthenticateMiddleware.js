const bcrypt = require('bcrypt');
const userProvider = require(__basedir + "/app/Providers/UserProvider");
const jwt = require('jsonwebtoken');

module.exports = function(req, res, next) {
    const authorization = req.header('Authorization');

    if(!authorization) {
        return res.status(403).json("You are not authorized to perform this request!");

    }

    const splittedauthorization = authorization.split(" ");
    const token = splittedauthorization[1];

    if(token === null) return res.status(401).json("You need to supply a token");

    jwt.verify(token, process.env.TOKEN_SECRET, (err, user) => {
        if (err) return res.status(403).json("You are not authorized to perform this request!");
        req.user = user;
        return next();
      })


}