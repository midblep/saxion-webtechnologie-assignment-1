const express = require('express');
const router = express.Router();

const userProvider = require(__basedir + "/app/Providers/UserProvider");
const bidProvider = require(__basedir + "/app/Providers/BidProvider");

const IsUser = require(__basedir + "/app/Middleware/IsUserMiddleware");
const Authenticate = require(__basedir + "/app/Middleware/AuthenticateMiddleware");


router.route('')
    .get(function (req, res) {
        userProvider.getAllUsers(function (err, users) {
            if (!err) {
                return res.status(200).json(users);
            } else {
                return res.status(400).json({ "error": err.message });
            }
        });
    })

    .post(function (req, res) {
        const newUser = req.body;

        if (!newUser.name || !newUser.email || !newUser.password || !newUser.gender ||
            newUser.name.length > 32 || newUser.email.length > 128 || newUser.gender.length > 16 ||
            newUser.name.length < 3 || newUser.email.length < 6 || newUser.gender.length < 3) {
            return res.status(400).json("Invalid Body");
        }

        var passwordFormat = /^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/;
        if(!newUser.password.match(passwordFormat)) return res.status(400).json("Password invalid")

        var mailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if(!newUser.email.match(mailFormat)) return res.status(400).json("Invalid e-mail")
        if(newUser.email.slice(-3) !== ".nl" && newUser.email.slice(-4) !== ".com") return res.status(400).json("E-mail must end with .com or .nl")

        userProvider.existsUserWithEmail(newUser.email, (result) => {
            if (result) {
                return res.status(400).json("A user with this email already exists!");
            } else {
                userProvider.createUser(newUser, (err) => {
                    if (!err) {
                        userProvider.getUserByEmail(newUser.email, (err, user) => {
                            return res.status(201).json(user);
                        });
                    }
                });
            }
        });
    });

router.route('/:id')
    .get(function (req, res) {
        if(!req.params.id) return res.status(400).json("You must supply an id!");

        userProvider.getUserById(req.params.id, (err, user) => {
            if (!err) {
                if(user !== undefined) {
                    return res.status(200).json(user);
                } else {
                    return res.status(400).json("A user with this ID does not exist!");
                }
            } else {
                return res.status(500).json("Internal Server Error");
            }
        });
    })

    .put(Authenticate, IsUser, function (req, res) {
        if(!req.params.id) return res.status(400).json("You must supply an id!");

        userProvider.getUserById(req.params.id, (err, user) => {
            if (!err) {

                const newUser = req.body;

                userProvider.updateUser(req.params.id, newUser, (err) => {
                    if (!err) {
                        return res.status(200).json("Updated User!");
                    } else {
                        return res.status(500).json(err);
                    }
                })
            } else {
                return res.status(500).json("Internal Server Error");
            }
        });
    });



module.exports = router