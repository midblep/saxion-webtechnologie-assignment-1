# Love Auctions

**DHI2V.Sa: Thom Sandbrink & Pascal van Ginkel**

![Saxion Logo](https://www.biobased-ukraine.nl/assets/uploads/sites/10/2018/08/Saxion.jpg)

### Opleverdatum: 01-11-2021

<div style="page-break-after: always;"></div>

## Inhoud

- [Love Auctions](#love-auctions)
    - [Opleverdatum: 01-11-2021](#opleverdatum-01-11-2021)
  - [Inhoud](#inhoud)
  - [1 Opstarten van de applicatie en concept](#1-opstarten-van-de-applicatie-en-concept)
  - [1.1 Opstarten van de applicatie](#11-opstarten-van-de-applicatie)
  - [Inloggen op front-end](#inloggen-op-front-end)
  - [1.2 Samenwerking Front-end en Back-end](#12-samenwerking-front-end-en-back-end)
  - [1.3 Component structuur](#13-component-structuur)
  - [1.4 Concept](#14-concept)
  - [4 Libraries](#4-libraries)
  - [5 Technische documentatie](#5-technische-documentatie)
    - [5.1 Tests Back-end](#51-tests-back-end)
    - [5.2 Tests Front-end](#52-tests-front-end)
    - [5.3 Back-end Structuur](#53-back-end-structuur)
    - [5.4 Front-end Structuur](#54-front-end-structuur)
  - [6 Functionaliteiten](#6-functionaliteiten)
    - [6.1 Functionaliteiten](#61-functionaliteiten)


<div style="page-break-after: always;"></div>

## 1 Opstarten van de applicatie en concept

## 1.1 Opstarten van de applicatie

De applicatie kan worden gestart doormiddel van het invoeren van de commandos `npm install` vervolgd door `npm start` in de console in beide de front-end en back-end. Ook is NodeJS 16 en NPM vereist.
Start front-end:
1. `cd client`
2. `npm install`
3. `npm start`
<br>
Start back-end:
1. `cd server`
2. `npm install`
3. `npm start`

## Inloggen op front-end
De volgende standaard gebruikers zijn aangemaakt voor inloggen:<br>
`Admin`:
Gebruikersnaam: admin
Wachtwoord: admin12345
<br>
`User`:
Gebruikersnaam: user
Wachtwoord: user123456
<br><br>
Ook kunt u zelf een account aanmaken op de registratie pagina.


## 1.2 Samenwerking Front-end en Back-end
De Front-end en Back-end werken samen doormiddel van fetch API calls. Met deze methode kan er gebruik gemaakt worden van de HTTP pipeline zoals requests en responses. HTTP geeft ook de mogelijkheid om gebruik te maken van status code. Op deze manier kan er makkelijk worden meegegeven aan een functie of deze is geslaagd of is mislukt.

## 1.3 Component structuur
In het Front-end hebben we gebruik gemaakt van meerdere componenten. Deze componenten hebben we per end point opgedeeld. Zo hebben bijvoorbeeld Auctions en Bids een eigen component. Hier is voor gekozen omdat er is gekozen voor een feature driven approach. De folder structuur is opgebouwd per feature.
Zo kan je makkelijk bugs oplossen omdat componenten die bij elkaar horen ook samen in 1 map zitten.

## 1.4 Concept

In deze applicatie kan je een date veilen. Dit betekend dat je als gebruiker een account kan aanmaken en als dit succesvol is gelukt kan de gebruiker inloggen. Als de gebruiker is ingelogd kan deze een aanvraag doen om een veiling aan te maken. Als een gebruiker dit doet veilt deze een date met hem/haar. Het idee is dat de gebruiker die de date veilt op date gaat met de winnaar van de veiling.*

Om een veiling te kunnen starten moet deze wel eerst goedgekeurd worden door een admin. Anders is het niet mogelijk om een veiling te starten.

<div style="page-break-after: always;"></div>

## 4 Libraries
In onze applicaties hebben wij de volgende libraries gebruikt:
- bcrypt

    De bcrypt librarie hebben wij gebruikt om wachtwoorden te encrypten zodat deze veilig kunnen worden verstuurd.
- cors

    De cors librarie hebben wij gebruikt omdat anders de browser de Front-end in bepaalde gevallen geen request zal versturen naar de Back-end server.
- dotenv

     De dotenv librarie hebben wij gebruikt voor de tokensecret. Deze tokensecret wordt gebruikt bij het encrypten van de JSONwebtokens.
- express

     De express librarie hebben wij gebruikt voor het opslaan van de data, de API end points op slaan en ook informatie uit de database kunnen halen.
- jsonwebtoken

     De jsonwebtoken librarie hebben wij gebruikt omdat dit een eis was van het vak. Daarnaast is het dit ook handig voor de beveiliging van de applicatie.
- sqlite3

     De sqlite3 librarie hebben wij gebruikt zodat er gebruik kan worden gemaakt van een database die start op het moment dat een gebruiker in de console "npm start" verstuurd.


<div style="page-break-after: always;"></div>

## 5 Technische documentatie

### 5.1 Tests Back-end
**Het is belangrijk dat de tests op een schone installatie worden uitgevoerd en in de juiste volgorde.**<br><br>
In de applicatie moet er natuurlijk het nodige getest worden. Om dit proces wat makkelijker te maken en te versnellen zijn er http testen geschreven. De volgende http testen zijn gecreëerd:
- Admintests: Deze tests worden uitgevoerd op API Endpoints die alleen een gebruiker met de rol ''admin'' kan gebruiken.
- Auctiontests: Deze tests worden uitgevoerd op de API Endpoints van de Auction (Veiling) feature.
- Bidtests: Deze tests worden uitgevoerd op de API Endpoints van de Bids (Biedingen) feature.
- Requesttests: Deze tests worden uitgevoerd op de API Endpoints van de Request (Aanvraag) feature.
- usertests: Deze tests worden uitgevoerd op de API Endpoints voor authenticatie zoals inloggen en registreren.

In deze testen worden de verschillende API endpoints getests. Deze endpoints bestaan uit HTTP's verbs:
 - GET
 - PUT
 - POST
 - DELETE

 Eerst wordt er een POST call uitgevoerd zodat er gegevens zijn om de test mee uit te voeren. Het object dat gemaakt wordt is het testopbject in de aankomende testen. Als het testobject is aangemaakt kan er gecontroleerd worden of ook de GET calls werken. Wanneer deze succesvol zijn wordt het test object aangepast door een PUT call. Daarna wordt het test object weer verwijderd door de DELETE call. Niet elk end point heeft alle calls. Zo hebben sommigen end points geen DELETE of PUT call.

De tests zullen in de volgende volgorde moeten worden uitgevoerd:
 - User
 - Admin
 - Request
 - Auction
 - Bid


### 5.2 Tests Front-end
**Het is belangrijk dat de back-end aan staat en werkt voor de front-end om goed te functioneren.**<br><br>
Het Front-end is getest doormiddel van user testen. Meerdere personen hebben de applicatie zorgvuldig getest op fouten. Dit is gedaan door bijvoorbeeld verkeerde waarden in te voeren.

### 5.3 Back-end Structuur
De API endpoints zijn te vinden op `localhost:3000/api`.
<br><br>
In de folderstructuur van de back-end hebben we Laravel's structuur overgenomen. (https://laravel.com/docs/8.x/structure)
Dit is omdat deze uitgebreid, veel gebruikt en duidelijk is. Alle applicatie's logica zit in de app folder, onderverdeeld in de diverse taken van de bestanden. De API endpoints worden in de routes folder beschreven.
<br><br>
Om je eigen gewonnen veilingen te vinden kan je de /api/auctions endpoint gebruiken en het wonBy query parameter gebruiken. Zoals: `/api/auctions?wonBy=1`.
<br><br>
We hebben de database standaard gevuld met wat voorbeelddata.

### 5.4 Front-end Structuur
De front-end website is te vinden op `localhost:5000`.
<br><br>
We maken gebruik van Svelte stores om bepaalde data zoals de JWT over een hele Request te kunnen gebruiken. Deze JWT stoppen we vervolgens in een cookie zodat deze over meerdere requests kan worden gebruikt en niet continu opnieuw moet worden ingevoerd door de gebruiker.


<div style="page-break-after: always;"></div>

## 6 Functionaliteiten

### 6.1 Functionaliteiten
Op onze pagina kan je gebruik maken van verschillende functionaliteiten.

Zo kan je op de hoofdpagina inloggen, regristreren en ook een overzicht bekijken van verschillende veilingen. Daarnaast is het ook mogelijk om de applicatie in "Dark modus te zetten".
Als je wilt regristreren zal je aan een aantal vereisten moeten doen. Voldoe je niet aan deze vereisten zal er een foutmelding worden gegeven en wordt je niet doorverwezen naar een andere pagina. Dit is ook het geval als je wilt inloggen met een account dat niet bekend is bij de server of als je met een verkeerd wachtwoord probeert in te loggen.

![InlogPagina](./Afbeeldingen/InlogPagina.png)

![DarkMode](./Afbeedlingen/DarkMode.png)

Als je inlogt komt de gebruiker op de homepagina. Daar kan deze naar het overzicht met aanvragen gaan of naar de veilingen.

![RequestAdmin](./Afbeedlingen/VeilingDetailpagina.png)

In de applicatie is het ook mogelijk om Auctions te bekijken. Je kan op verschillende eigenschappen filteren. Als je geregristreerd bent kan je ook een bod uitbrengen op een veiling. Als de gebruiker een veiling heeft gewonnen dan ziet deze dat op zijn veilingspagina.

![Auctions](./Afbeedlingen/VeilingDetailpagina.png)

![Auctions](./Afbeedlingen/VeilingLijst.png)

Een veiling moet door een gebruiker worden aangevraagd. Deze kan vervolgens goedgekeurd worden maar ook worden afgekeurd door een admin.

![RequestUser](./Afbeedlingen/VeilingAanvraag.png)

![RequestAdminEdit](./Afbeedlingen/AdminVeilingBewerken.png)

![RequestAdmin](./Afbeedlingen/AdminAanvragenLijst.png)

![RequestAdminPanel](./Afbeedlingen/AdminPaneel.png)







