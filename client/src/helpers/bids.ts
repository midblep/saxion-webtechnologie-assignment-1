export async function getBidDetails(bid: Bid): Promise<Bid> {
    const bidcreator = await fetch(`http://localhost:3000/api/users/${bid.created_by}`, {
        method: "GET"
    });
    const creator: User = await bidcreator.json();
    bid.creator = creator;

    const bidauction = await fetch(`http://localhost:3000/api/auctions/${bid.auction_id}`, {
        method: "GET"
    });
    const auction: Auction = await bidauction.json();
    bid.auction = auction;

    return bid;
}