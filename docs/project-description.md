# **Love Auctions**™
## Buy your loved ones... success guaranteed*
**WITH ANTI-KIDNAPPING FEATURES NOW FOR ONLY €4,99 A MONTH****

## **Introduction**
  - [**Introduction**](#introduction)
  - [**Overall Features**](#overall-features)
  - [**Models**](#models)
  - [**Short overview of needed CRUDs**](#short-overview-of-needed-cruds)
  - [**Stack**](#stack)

## **Overall Features**
- Exeptional clothes and deoderant is required for registration
- Percentage of winning bidder amount goes to Love Auctions
- Auctions for getting a date with a user
  - Auctions have starting bids, increase bids and acceptance bids
  - Bidders remain anonymous during bidding
  - Auctioner can set a setting that allows bidders to also remain anonymous during chatting (and bidders can see this before bidding)
- Highest bidder wins
  - If highest bidder cant pay, it goes to second highest and highest bidder gets a red strike on their profile (message: WARNING THIS GUY IS POOR)
- Success stories (sorta like reviews) for when a date turned into a relationship
- When chatting either party can make a date with a time/place. When both mark the date as done, it allows them to review and closes the chat (warns them to get each other's number first).
  - Stories can be shown on the site to prove the site's effectiveness and signal to people the account is not up for auctions right now.
- Ratings on accounts (both users can rate after the date or if the date hasn't been done) to prove legitimacy.
- Give a user a heart to ''follow'', hearts can artificially create a sense of important to drive up bids
- After auctioning, you can chat with the user you won the auction from
- Profiles have all the standard Tinder things such as location
- Report button for human trafficking and auctioners scamming on the site
- Users have 1000 Love Credits on registration, which they use to bid as currency. You can get more Love Credits by paying money, but for this assignment we will give people more Love Credits by just doing an auction and getting money from the highest bidder.
- Basic authentication features such as login
- Search and listings to find auctions based on features such as:
  - Highest bid
  - Minimum bid
  - Amount of bids
  - Soon to expire
  - Title/description
  - All from user
  - Closest to you
  - Auctioner's amount of hearts

## **Models**
- Users (description, age, etc)
  - Hearts
  - Ratings
  - Strike
- Auction (sb, ib, ab)
  - Bid
  - Deadline
- Chat
- Media
- Success Story
- Report
- Date
    - Date of the date

## **Short overview of needed CRUDs**
- Media || Must Have
  - Create a media (upload picture anywhere)
  - Update a media (change the name)
  - Delete a media (remove the picture)
  - Read a media (look at picture)

- Users || Must Have
  - Create User (register)
  - Change User Details (profile fields)
  - Delete User Account (GDPR compliance)
  - Look at Users (read)

- Date || Must Have
  - Create a date (bidder and auctioner can)
  - Update a date (when its been done, this signals review and closes chat, both have to mark it done for it to mark as done)
  - Read a date (view)

- Auction || Must Have
  - Read an auction (view)
  - Create an new auction (set start date in future)
  - Update an auction (change details, unless active, and make ''hidden/expired'')

- Bid || Must Have
  - Create a new bid (for auction)
  - Read a bid

- Ratings || Should Have
  - Create a new rating
  - Read the average rating (avg based on all rating models for a given user)
  - Update the rating you gave someone (amount stars)
  - See the ratings of profiles (Read)

- Success Stories || Should Have
  - Create a new success story (link potential success stories of bidder and auctioner together)
  - Delete a previous success story (can only have one)
  - Update the text on a posted success stories (typos)
  - Read a success story

- Chat || Should Have
  - Read a chat (view)
  - Create a chat (automatically when winning auction)
  - Update a chat (make it hidden or closed when date ended)

- ChatMessage || Should Have
  - Create a new message
  - Read a message

- Strike || Could Have
  - Read a strike (other CRUDs only internal logic)

- Hearts || Could Have
  - Create a heart (give someone heart)
  - Read a heart (see someone's total hearts)
  - Delete a heart (remove the (virtual)heart from someone)

- Report || Could Have
  - Create a new report


## **Stack**
**Back-end**

ExpressJS on NodeJS with JSON/MySQL

**Front-end**

Svelte



\* not really

\** promotion applies to first month only, after that €19,99