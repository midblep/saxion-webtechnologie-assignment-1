// global variables
global.__basedir = __dirname;

// imports
const dotenv = require('dotenv');
const express = require('express');
const cors = require('cors');
const app = express();

// dotenv
dotenv.config();

// cors
let corsOptions = {
    origin: "http://localhost:5000",
    optionsSuccessStatus: 200,
}
app.use(cors(corsOptions));
app.options('*', cors());

// global middleware
app.use(express.json());

// init db
const db = require(__basedir + "/app/Services/DataService");
db.initialize();

// variables
const port = process.env.PORT || 3000;

// initialize routes
app.use('/', require(__basedir + "/routes/index"));

// start webserver
app.listen(port, () => {
    console.log(`Running on localhost:${port}`);
});