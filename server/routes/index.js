const express = require('express');
const router = express.Router();

router.use('/api/auctions', require(__basedir + '/app/Controllers/AuctionController'));
router.use('/api/bids', require(__basedir + '/app/Controllers/BidController'));
router.use('/api/users', require(__basedir + '/app/Controllers/UserController'));
router.use('/api/requests', require(__basedir + '/app/Controllers/RequestController'));
router.use('/api/get_token', require(__basedir + '/app/Controllers/AuthController'));

module.exports = router;