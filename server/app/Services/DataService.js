const sqlite3 = require('sqlite3').verbose();
const bcrypt = require('bcrypt');
const fs = require('fs');

const dbFilePath = __basedir + "/database/data.sqlite";
const resetDatabase = true;
const dummyData = true;


//
// first time startup
//

function generateTables() {
    let db = new sqlite3.Database(dbFilePath, (err) => {
        if (err) { console.error(err); } else {
            db.run(`CREATE TABLE users (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                name VARCHAR(32) NOT NULL,
                email VARCHAR(64) UNIQUE NOT NULL,
                password VARCHAR(64) NOT NULL,
                gender VARCHAR(32) NOT NULL,
                role VARCHAR(32) NOT NULL DEFAULT 'user',
                CONSTRAINT email_unique UNIQUE (email)
                );`, (err) => {
                    if(err) {
                        console.error(err)
                    } else {
                        if(dummyData) {
                            let insertUsers = 'INSERT INTO users (name, email, password, gender, role) VALUES (?,?,?,?,?)';
                            bcrypt.hash("user123456", 10, function (err, hash) {
                                db.run(insertUsers, ["user", "user@example.com", hash, "male", "user"]);
                            });
                            bcrypt.hash("admin12345", 10, function (err, hash) {
                                db.run(insertUsers, ["admin", "admin@example.com", hash, "male", "admin"]);
                            });
                        }
                    }
                });

            db.run(`CREATE TABLE auctions (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                title VARCHAR(32) NOT NULL,
                description TEXT NOT NULL,
                end_date INTEGER NOT NULL,
                user_id INTEGER NOT NULL,
                created_by INTEGER NOT NULL,
                FOREIGN KEY (user_id)
                    REFERENCES users (id)
                    ON DELETE CASCADE
                FOREIGN KEY (created_by)
                    REFERENCES users (id)
                    ON DELETE CASCADE
                );`, (err) => {
                    if(err) {
                        console.error(err)
                    } else {
                        if(dummyData) {
                            let insertAuctions = 'INSERT INTO auctions (title, description, end_date, user_id, created_by) VALUES (?,?,?,?,?)';
                            db.run(insertAuctions, ["Neem een date met user", "Neem deze prachtige kans op een geweldige date met de enige echte user!!", Date.now() + 10000000, 2, 1]);
                            db.run(insertAuctions, ["Neem een date met user", "Neem deze prachtige kans op een geweldige date met de enige echte user!!", Date.now() + 10000000, 2, 1]);
                            db.run(insertAuctions, ["Neem een date met user", "Neem deze prachtige kans op een geweldige date met de enige echte user!!", Date.now() + 10000000, 2, 1]);
                            db.run(insertAuctions, ["Neem een date met user", "Neem deze prachtige kans op een geweldige date met de enige echte user!!", Date.now(), 2, 1]);
                            db.run(insertAuctions, ["Neem een date met user", "Neem deze prachtige kans op een geweldige date met de enige echte user!!", Date.now(), 2, 1]);
                            db.run(insertAuctions, ["Neem een date met user", "Neem deze prachtige kans op een geweldige date met de enige echte user!!", Date.now() + 10000000, 2, 1]);
                        }
                    }
                });

            db.run(`CREATE TABLE bids (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                auction_id INTEGER NOT NULL,
                amount INTEGER NOT NULL,
                created_at INTEGER NOT NULL,
                created_by INTEGER NOT NULL,
                FOREIGN KEY (auction_id)
                    REFERENCES auctions (id)
                    ON DELETE CASCADE
                FOREIGN KEY (created_by)
                    REFERENCES users (id)
                    ON DELETE CASCADE
                );`, (err) => {
                    if(err) {
                        console.error(err)
                    } else {
                        if(dummyData) {
                            let insertBids = 'INSERT INTO bids (auction_id, amount, created_at, created_by) VALUES (?,?,?,?)';
                            db.run(insertBids, [1, 1, Date.now(), 1]);
                            db.run(insertBids, [2, 2, Date.now(), 1]);
                            db.run(insertBids, [2, 10, Date.now(), 1]);
                            db.run(insertBids, [2, 150, Date.now(), 1]);
                            db.run(insertBids, [3, 10, Date.now(), 1]);
                            db.run(insertBids, [3, 20, Date.now(), 1]);
                        }
                    }
                });

            db.run(`CREATE TABLE requests (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                approved BOOLEAN NOT NULL,
                created_at INTEGER NOT NULL,
                created_by INTEGER NOT NULL,
                FOREIGN KEY (created_by)
                    REFERENCES users (id)
                    ON DELETE CASCADE
                );`, (err) => {
                    if(err) {
                        console.error(err)
                    } else {
                        if(dummyData) {
                            let insertRequests = 'INSERT INTO requests (approved, created_at, created_by) VALUES (?,?,?)';
                            db.run(insertRequests, [false, Date.now(), 2]);
                        }
                    }
                });
        }
    });
}

function deleteDB() {
    fs.unlinkSync(dbFilePath)
}

module.exports.initialize = function () {
    if(resetDatabase) {
        deleteDB();
        console.log('Dropped all tables!');
        generateTables();
        console.log('Regenerated all tables!');
    }
}


//
// connection logic
//

module.exports.connect = new sqlite3.Database(dbFilePath, (err) => {
    if (err) {
        return console.error(err.message);
    }
});