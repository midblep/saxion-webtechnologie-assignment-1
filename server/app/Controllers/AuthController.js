const express = require('express');
const router = express.Router();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const userProvider = require(__basedir + "/app/Providers/UserProvider");

router.route('')
    .post(function (req, res) {
        const name = req.body.name ?? undefined;
        const password = req.body.password ?? undefined;

        if (name === undefined || password === undefined) {
            return res.status(403).json("You must define a name and password to get a token!");
        }

        userProvider.getUserByName(name, function (err, user) {
            if (user) {
                bcrypt.compare(password, user.password, function (err, result) {
                    if (result == true) {
                        const token = jwt.sign(user, process.env.TOKEN_SECRET);
                        return res.status(201).json(token);
                    } else {
                        return res.status(401).json("Invalid credentials!");
                    }
                });
            } else {
                return res.status(401).json("You are not authorized to perform this request!");
            }
        });
    });



module.exports = router