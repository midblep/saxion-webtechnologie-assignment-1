const db = require(__basedir + "/app/Services/DataService").connect;
const bcrypt = require('bcrypt');

module.exports.getUserById = function(id, callback) {
    db.get(`select * from users where id = '${id}'`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.getUserByIdPromised = async function(id) {
    return new Promise((resolve, reject) => {
        db.get(`select * from users where id = '${id}'`, [], (err, user) => {
            if(err) return reject(err);
            resolve(user);
        });
    })
}

module.exports.getUserByName = function(name, callback) {
    db.get(`select * from users where name = '${name}'`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.getUserByEmail = function(email, callback) {
    db.get(`select * from users where email = '${email}'`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.getUserByGender = function(gender, callback) {
    db.get(`select * from users where gender = '${gender}'`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.existsUserWithEmail = function(email, callback) {
    db.get(`select * from users where email = '${email}'`, [], (err, user) => {
        if(!err) {
            if(user) callback(true);
            callback(false);
        } else {
            callback(err);
        }
    });
}

module.exports.getAllUsers = function(callback) {
    db.all(`select * from users`, [], (err, user) => {
        callback(err, user);
    });
}

module.exports.createUser = function(newUser, callback) {
    let name = newUser.name;
    let email = newUser.email;
    let password = newUser.password;
    let gender = newUser.gender;
    let role = "user";
    bcrypt.hash(password, 10, function(err, hash) {
        if(!err) {
            db.run(`INSERT INTO users (name, email, password, gender, role) VALUES (?,?,?,?,?)`, [name, email, hash, gender, role], (err) => {
                callback(err);
            });
        }
    });
}

module.exports.updateUser = function(id, newUserArray, callback) {
    this.getUserById(id, (err, result) => {
        if(!err) {
            const user = result;

            if(newUserArray.password) {
                bcrypt.hash(newUserArray.password, 10, function(err, hash) {
                    if(!err) {
                        db.run(`UPDATE users SET
                            name = '${newUserArray.name ? newUserArray.name : user.name}',
                            email = '${newUserArray.email ? newUserArray.email : user.email}',
                            gender = '${newUserArray.gender ? newUserArray.gender : user.gender}',
                            password = '${hash ? hash : user.email}'
                            WHERE id = '${id}'`,
                        (err) => {
                            callback(err);
                        });
                    }
                });
            }
        }
    });
}