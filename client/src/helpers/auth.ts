import { tokenStore } from "./store";

export function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
}

export function parseJwt(token) {
    const base64Url = token.split('.')[1];
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/');
    const jsonPayload = decodeURIComponent(atob(base64).split('').map(function (c) {
        return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);
    }).join(''));

    return jsonPayload;
}

export async function getUserFromEmail(userObject: string) {
    const user: User = JSON.parse(userObject);
    const response = await fetch('http://localhost:3000/api/users/' + user.id);
    return await response.json();
}

export async function getUser() {
    const email = parseJwt(getCookie('token'));
    const user = await getUserFromEmail(email);
    return user;
}

export async function logOut() {
    tokenStore.set(null);
    location.replace('/');
}