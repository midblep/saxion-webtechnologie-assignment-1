/// <reference types="svelte" />

interface AppRequest {
    id: number,
    approved: boolean,
    created_at: number,
    created_by: number,
    creator?: User,
}

interface Auction {
    id: number,
    title: string,
    description: string,
    end_date: number,
    user_id: number,
    user?: User,
    created_by: number,
    creator?: User,
    bids?: Bid[],
}

interface Role {
    user,
    admin,
}

interface User {
    id: number,
    name: string,
    email: string,
    password: string,
    gender: string,
    role: Role,
}

interface Bid {
    id: number,
    auction_id: number,
    auction?: Auction,
    amount: number,
    created_at: number,
    created_by: number,
    creator?: User,
}