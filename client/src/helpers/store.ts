import {writable} from "svelte/store";
import { getCookie, getUserFromEmail, parseJwt } from "./auth";


//
// Storing the user's token when logged in
//

var authorizationToken = getCookie('token');
var tokenStore = writable(authorizationToken);

tokenStore.subscribe(value => {
    if (value != null) {
        document.cookie = "token=" + value + ";path=/;";
    } else {
        document.cookie = "token=;path=/;expires=Thu, 01 Jan 1970 00:00:00 UTC;";
    }
});


//
// store if the user is logged in
//

var isAuthenticated = false;

if (authorizationToken) {
    isAuthenticated = true;
}

var isLoggedIn = writable(isAuthenticated);


//
// exports
//

export {tokenStore as tokenStore};
export {isLoggedIn as isLoggedIn};