const express = require('express');
const router = express.Router();

const bidProvider = require(__basedir + "/app/Providers/BidProvider");
const auctionProvider = require(__basedir + "/app/Providers/AuctionProvider");

const IsUser = require(__basedir + "/app/Middleware/IsUserMiddleware");
const Authenticate = require(__basedir + "/app/Middleware/AuthenticateMiddleware");


// routes for this controller
router.route('')
    .get(function (req, res) {
        const auction_id = req.query.auction_id ?? undefined;

        if(auction_id) {
            bidProvider.getAllBidsForAuction(auction_id, function (err, bids) {
                if (!err) {
                    return res.status(200).json(bids);
                } else {
                    return res.status(500).json(err);
                }
            });
        } else {
            bidProvider.getAllBids(function (err, bids) {
                if (!err) {
                    return res.status(200).json(bids);
                } else {
                    return res.status(500).json(err);
                }
            });
        }
    })

    .post(function (req, res) {
        const newBid = req.body;

        if (!newBid.auction_id || !newBid.amount || !newBid.created_by || !newBid.created_at) return res.status(400).json("Invalid body");

        auctionProvider.getAuctionById(newBid.auction_id, (err, result) => {
            bidProvider.getAllBidsForAuction(result.id, (err, bids) => {
                let largestBid = 0;

                bids.forEach((element) => {
                    if(element.amount > largestBid){
                        largestBid = element.amount;
                    }
                });

                if(err) {
                    return res.status(500).json("Internal Server Error");
                }
                else if(result == null) {
                    return res.status(400).json("The auction this bid is for must exist!");
                }
                else if(result.end_date == undefined || result.end_date < Date.now()) {
                    return res.status(400).json("This auction has ended, you can no longer add a bid to it!");
                }else if(newBid.amount <= largestBid){
                    return res.status(400).json("Must be higher than other bids");
                }else {
                    bidProvider.createBid(newBid, (err, bid) => {
                        if (!err && bid !== null) {
                            return res.status(201).json(bid);
                        } else {
                            return res.status(500).json(err);
                        }
                    });
                }
            });
        });
    });

router.route('/:id')
    .get(Authenticate, IsUser, function (req, res) {
        const id = req.params.id;

        bidProvider.getBidById(id, (err, bid) => {
            if (!err) {
                return res.status(200).json(bid);
            } else {
                return res.status(400).json(`No bid with id of ${id} found!`);
            }
        });
    })

    .delete(Authenticate, function (req, res) {
        const id = req.params.id ?? null;
        const userId = req.user.id;
        const isAdmin = req.user.role == "admin" ? true : false;

        bidProvider.getBidById(id, function (err, bid) {
            if (!err && bid !== undefined) {
                if(isAdmin || userId == bid.created_by) {
                    bidProvider.deleteBid(bid.id, (err) => {
                        if (!err) {
                            return res.status(200).json("Deleted");
                        } else {
                            return res.status(400).json(err);
                        }
                    });
                } else {
                    return res.status(400).json("You are not authorized to delete this bid!");
                }
            } else {
                return res.status(400).json("A bid with this id does not exist!");
            }
        });
    });

module.exports = router