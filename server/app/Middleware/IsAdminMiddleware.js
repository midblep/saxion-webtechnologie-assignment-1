const db = require(__basedir + "/app/Services/DataService").connect;
const bcrypt = require('bcrypt');
const userProvider = require(__basedir + "/app/Providers/UserProvider");

module.exports = function(req, res, next) {
    const user = req.user;

    if(user !== null) {
        userProvider.getUserById(user.id, function (err, foundUser) {
            if(foundUser && foundUser.role === "admin") {
                return next();
            } else {
                return res.status(403).json("You are not authorized to perform this request!");
            }
        });
    } else {
        return res.status(403).json("You are not authorized to perform this request!");
    }
};