{
  "swagger": "2.0",
  "info": {
    "description": "Example description is an example description that makes for a good example description.",
    "version": "0.0.1",
    "title": "Love Auctions"
  },
  "host": "localhost",
  "basePath": "/api",
  "tags": [
    {
      "name": "users",
      "description": "Website Users"
    },
    {
      "name": "auctions",
      "description": "Auctions on the website"
    },
    {
      "name": "requests",
      "description": "A request to the site admin to make an auction for a user"
    },
    {
      "name": "bids",
      "description": "Monetary bids on the auctions"
    }
  ],
  "schemes": [
    "http"
  ],
  "paths": {
    "/users": {
      "get": {
        "tags": [
          "users"
        ],
        "summary": "Get all the users of the website",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "name",
            "type": "string",
            "description": "Filter user results by specific name string"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok"
          },
          "404": {
            "description": "No users (with the given parameters) found"
          }
        }
      },
      "post": {
        "tags": [
          "users"
        ],
        "summary": "Register a new user to the website",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "The new user that needs to be added",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created"
          },
          "405": {
            "description": "Invalid input"
          }
        }
      }
    },
    "/users/{id}": {
      "get": {
        "tags": [
          "users"
        ],
        "summary": "Get a specific user on the website",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the user"
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          },
          "404": {
            "description": "No users with this ID found"
          }
        }
      },
      "put": {
        "tags": [
          "users"
        ],
        "summary": "Change details about a specific user",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the user to change"
          },
          {
            "in": "body",
            "name": "body",
            "description": "New details about the user you're editing",
            "required": true,
            "schema": {
              "$ref": "#/definitions/User"
            }
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          },
          "404": {
            "description": "No users with this ID found"
          },
          "405": {
            "description": "Invalid body content structure"
          }
        }
      },
      "delete": {
        "tags": [
          "users"
        ],
        "summary": "Delete a given user by their ID",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the user to delete"
          }
        ],
        "responses": {
          "200": {
            "description": "Deleted"
          },
          "404": {
            "description": "No users with this ID found"
          }
        }
      }
    },
    "/auctions": {
      "get": {
        "tags": [
          "auctions"
        ],
        "summary": "Get all the auctions on the site",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "isRunning",
            "type": "boolean",
            "description": "Only get auctions that are either closed or open"
          },
          {
            "in": "query",
            "name": "userAge",
            "type": "string",
            "description": "The age of the user attached to the date that is being auctioned"
          },
          {
            "in": "query",
            "name": "userGender",
            "type": "string",
            "description": "The gender of the user attached to the date that is being auctioned"
          },
          {
            "in": "query",
            "name": "userHeartsMin",
            "type": "string",
            "description": "The minimum amount of hearts a user that is being auctioned needs to have"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok"
          },
          "404": {
            "description": "No auctions (with the given parameters) found"
          }
        }
      },
      "post": {
        "tags": [
          "auctions"
        ],
        "summary": "Create a new auction on the site",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "The auction object that will be made",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Auction"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created"
          },
          "405": {
            "description": "Invalid input"
          }
        }
      }
    },
    "/auctions/{id}": {
      "put": {
        "tags": [
          "auctions"
        ],
        "summary": "Change details about a specific auction",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the auction to change"
          },
          {
            "in": "body",
            "name": "body",
            "description": "New details about the auction you're editing",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Auction"
            }
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          },
          "404": {
            "description": "No auction with this ID found"
          },
          "405": {
            "description": "Invalid body content structure"
          }
        }
      }
    },
    "/bids": {
      "get": {
        "tags": [
          "bids"
        ],
        "summary": "Get all the bids placed on the site",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "minAmount",
            "type": "string",
            "description": "The maximum amount of money the bid has"
          },
          {
            "in": "query",
            "name": "maxAmount",
            "type": "string",
            "description": "The minimum amount of money the bid has"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok"
          },
          "404": {
            "description": "No bids (with the given parameters) found"
          }
        }
      },
      "post": {
        "tags": [
          "bids"
        ],
        "summary": "Create a new bid",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "The new bid that needs to be added",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Bid"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created"
          },
          "405": {
            "description": "Invalid input"
          }
        }
      }
    },
    "/bids/{id}": {
      "get": {
        "tags": [
          "bids"
        ],
        "summary": "Get a specific bid placed on the site",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the bid"
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          },
          "404": {
            "description": "No bids with this ID found"
          }
        }
      },
      "delete": {
        "tags": [
          "bids"
        ],
        "summary": "Delete a given bid by their ID",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the bid to delete"
          }
        ],
        "responses": {
          "200": {
            "description": "Deleted"
          },
          "404": {
            "description": "No bid with this ID found"
          }
        }
      }
    },
    "/requests": {
      "get": {
        "tags": [
          "requests"
        ],
        "summary": "Get all the requests placed on the site",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "query",
            "name": "isAccepted",
            "type": "boolean",
            "description": "Only get requests that are either pending or not pending"
          }
        ],
        "responses": {
          "200": {
            "description": "Ok"
          },
          "404": {
            "description": "No requests found"
          }
        }
      },
      "post": {
        "tags": [
          "requests"
        ],
        "summary": "Create a new request",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "body",
            "name": "body",
            "description": "The new request that needs to be created",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Request"
            }
          }
        ],
        "responses": {
          "201": {
            "description": "Created"
          },
          "405": {
            "description": "Invalid input"
          }
        }
      }
    },
    "/requests/{id}": {
      "get": {
        "tags": [
          "requests"
        ],
        "summary": "Get a specific requests placed on the site",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the requests"
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          },
          "404": {
            "description": "No requests with this ID found"
          }
        }
      },
      "put": {
        "tags": [
          "requests"
        ],
        "summary": "Change details about a specific request",
        "consumes": [
          "application/json"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "in": "path",
            "name": "id",
            "type": "integer",
            "required": true,
            "description": "ID of the request to change"
          },
          {
            "in": "body",
            "name": "body",
            "description": "New details about the request you're editing",
            "required": true,
            "schema": {
              "$ref": "#/definitions/Request"
            }
          }
        ],
        "responses": {
          "400": {
            "description": "Invalid ID supplied"
          },
          "404": {
            "description": "No request with this ID found"
          },
          "405": {
            "description": "Invalid body content structure"
          }
        }
      }
    }
  },
  "definitions": {
    "Request": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "approved": {
          "type": "boolean",
          "default": false
        },
        "created_by": {
          "type": "integer",
          "format": "int64"
        },
        "created_at": {
          "type": "string"
        }
      }
    },
    "Auction": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "name": {
          "type": "string"
        },
        "user": {
          "type": "integer",
          "format": "int64",
          "description": "Attached user"
        },
        "status": {
          "type": "string"
        },
        "finishes_at": {
          "type": "string"
        },
        "created_by": {
          "type": "integer",
          "format": "int64"
        },
        "created_at": {
          "type": "string"
        }
      }
    },
    "Bid": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "auction": {
          "type": "integer",
          "format": "int64"
        },
        "amount": {
          "type": "integer",
          "format": "int64"
        },
        "created_by": {
          "type": "integer",
          "format": "int64"
        },
        "created_at": {
          "type": "string"
        }
      }
    },
    "User": {
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "format": "int64"
        },
        "username": {
          "type": "string"
        },
        "email": {
          "type": "string"
        },
        "password": {
          "type": "string"
        },
        "gender": {
          "type": "string",
          "enum": [
            "male",
            "female",
            "undisclosed"
          ]
        },
        "role": {
          "type": "string",
          "enum": [
            "user",
            "admin",
            "undisclosed"
          ],
          "default": "user"
        }
      }
    }
  }
}