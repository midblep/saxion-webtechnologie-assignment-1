const express = require('express');
const router = express.Router();
const AuthenticateMiddleware = require(__basedir + '/app/Middleware/AuthenticateMiddleware');
const IsAdminMiddleware = require(__basedir + '/app/Middleware/IsAdminMiddleware');
const IsUserMiddleware = require(__basedir + '/app/Middleware/IsUserMiddleware');
const auctionProvider = require(__basedir + "/app/Providers/AuctionProvider");
const bidProvider = require(__basedir + "/app/Providers/BidProvider");
const userProvider = require(__basedir + "/app/Providers/UserProvider");

// routes for this controller
router.route('')
    .get(async function (req, res) {
        const search = req.query.search ?? undefined;
        const userGender = req.query.userGender ?? undefined;
        const maxPrice = req.query.maxPrice ?? undefined;
        const endDate = req.query.endDate ?? undefined;
        const wonBy = req.query.wonBy ?? undefined;
        const page = req.query.page ?? 1;
        const perPage = 5;

        let auctions = await auctionProvider.getAllAuctionsPromised();

        if(search) {
            auctions.forEach((element, index, object) => {
                if(element.title !== search && element.description !== search) {
                    object.splice(index, 1);
                }
            });
        }

        if(endDate) {
            let newAuctions = [];
            for(const value of auctions) {

                if(value.end_date < endDate) {
                    newAuctions.push(value);
                }
            };
            auctions = newAuctions;
        }

        if(wonBy) {
            let newAuctions = [];
            for(const value of auctions) {
                if(value.end_date < Date.now()) {
                    let highestBid = 0;
                    let highestBidder;
                    let bids = await bidProvider.bidsFromAuctionPromised(value.id);

                    bids.forEach(bid => {
                        if(bid.amount > highestBid) {
                            highestBid = bid.amount;
                            highestBidder = bid.created_by;
                        }
                    });

                    if(highestBidder == wonBy) {
                        newAuctions.push(value);
                    }
                }
            }
            auctions = newAuctions;
        }

        if(userGender) {
            let newAuctions = [];
            for(const value of auctions) {
                let user = await userProvider.getUserByIdPromised(value.user_id);

                if(user.gender === userGender) {
                    newAuctions.push(value);
                }
            }
            auctions = newAuctions;
        }

        if(maxPrice) {
            let newAuctions = [];
            for(const value of auctions) {
                let highestBid = 0;
                let bids = await bidProvider.bidsFromAuctionPromised(value.id);

                bids.forEach(bid => {
                    if(bid.amount > highestBid) {
                        highestBid = bid.amount;
                    }
                });

                if(highestBid <= maxPrice) {
                    newAuctions.push(value);
                }
            }
            auctions = newAuctions;
        }

        let start = (page - 1) * perPage;
        let end = page * perPage;

        if(auctions.length > end) {
            auctions = auctions.slice(start, end);
        } else {
            auctions = auctions.slice(start, auctions.length);
        }

        return res.status(200).json(auctions);
    })

    .post(AuthenticateMiddleware, IsAdminMiddleware, function (req, res) {
        const newAuction = req.body;
        const user = req.user;

        if (!newAuction.title || !newAuction.description || !newAuction.end_date || !newAuction.user_id) {
            return res.status(400).json("Invalid Body");
        }
        newAuction.created_by = user.id;

        auctionProvider.createAuction(newAuction, (err, auction) => {
            if(!err && auction !== undefined) return res.status(201).json(auction);
            return res.status(500).json("Internal server error");
        });
    });

// routes for this controller
router.route('/:id')
    .get(function (req, res) {
        auctionProvider.getAuctionById(req.params.id, function(err, user) {
            if(!err) {
                return res.status(200).json(user);
            } else {
                return res.status(400).json("An auction with this id does not exist!");
            }
        });
    })

    .put(AuthenticateMiddleware, IsAdminMiddleware, function (req, res) {
        auctionProvider.getAuctionById(req.params.id, function(err, auction) {
            if(!err) {
                if(req.body.title) auction.title = req.body.title;
                if(req.body.description) auction.description = req.body.description;
                if(req.body.end_date) auction.end_date = req.body.end_date;
                if(req.body.user_id) auction.user_id = req.body.user_id;

                auctionProvider.editAuction(auction, (err, auction) => {
                    if(!err && auction !== undefined) return res.status(200).json(auction);
                    return res.status(500).json(auction);
                });
            } else {
                return res.status(400).json("An auction with this id does not exist!");
            }
        });
    })

    .delete(AuthenticateMiddleware, IsAdminMiddleware, function (req, res) {
        auctionProvider.getAuctionById(req.params.id, function(err, auction) {
            if(!err) {
                auctionProvider.deleteAuction(auction.id, (err) => {
                    if(!err) return res.status(200).json("Deleted");
                });
            } else {
                return res.status(400).json("An auction with this name does not exist!");
            }
        });
    });

router.route('/:id/bids')
    .get(function (req, res) {
        const largestOnly = req.query.largestOnly ?? false;
        const auction_id = req.params.id;

        if(!auction_id) return res.status(400).json("You must give an auction ID!");

        if(largestOnly) {
            bidProvider.getAllBidsForAuction(auction_id, function (err, bids) {
                let largestBid;
                let largestAmount = 0;

                bids.forEach((element) => {
                    if(element.amount > largestAmount){
                        largestBid = element;
                        largestAmount = element.amount;
                    }
                });
                if (!err) {
                    return res.status(200).json(largestBid);
                } else {
                    return res.status(500).json("Internal Server Error");
                }
            });
        } else {
            bidProvider.getAllBidsForAuction(auction_id, function (err, bids) {
                if(!err) return res.status(200).json(bids);
                return res.status(500).json("Internal Server Error");
            });
        }

    });

module.exports = router